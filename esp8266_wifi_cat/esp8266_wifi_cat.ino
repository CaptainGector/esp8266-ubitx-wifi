#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>

// Rx, Tx
SoftwareSerial SoftSerial(4, 5, false);

// Our web server for CAT control
WiFiServer server(80);

char ssid[] = "YourSSID";
char password[] = "YourWifiPassword";

// Our (theoretical) myClient
WiFiClient myClient;

// Blink code stuff
bool ledState = false;
int blinkTime = 250; // Milliseconds
int lastBlink = 0;

void setup() {
	pinMode(2, OUTPUT); // LED pin
	pinMode(2, HIGH); // Turn off.

	delay(100);

	// Establish a web-server for recieving/transmitting CAT commands.
	// Act as a bi-directional serial passthrough using wifi
	WiFi.begin(ssid, password); 
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		digitalWrite(2, ledState);
		ledState = !ledState;
	}

	server.begin();
	server.setNoDelay(true);

	// Software serial port
	// define pin modes for tx, rx:
	//pinMode(2, INPUT);
	//pinMode(3, OUTPUT);
	SoftSerial.begin(38400);
	pinMode(2, HIGH); // Turn LED
}	

void loop() {

	// Process blink and check if we're connected
	if (WiFi.status() == WL_CONNECTED) {
		//digitalWrite(2, LOW); // LED ON
		//ledState = true;
	} else if ((millis() - lastBlink) > blinkTime) {
		ledState = !ledState;
		digitalWrite(2, ledState);
		lastBlink = millis();
	}

	// If we ARE disconnected, attempt to reconnect
	if (WiFi.status() != WL_CONNECTED) {
		WiFi.begin(ssid, password);
		while (WiFi.status() != WL_CONNECTED)
		{
			// Slow blink
			delay(500);
			digitalWrite(2, ledState);
			ledState = !ledState;
		}
	}

	// Handle wifi myClient
	if (myClient && myClient.connected()) {
		// We have a valid myClient. 
		// Pass data from UBitX to computer
		while (myClient.available()) {
			SoftSerial.write(myClient.read());
		}

		// Pass data from computer to uBitX
		while (SoftSerial.available() > 0 ) {
			myClient.write(SoftSerial.read());
		}
		// Blink really slowly to tell the client we're good.
		if (millis() - lastBlink > 1000) {
			ledState = !ledState; 
			digitalWrite(2, ledState);
			lastBlink = millis();
		}
		
	} else {
		// Bye bye!
		myClient.stop();

		// We don't have a myClient!
		// Try and get one.
		myClient = server.available();
	}
	
}
